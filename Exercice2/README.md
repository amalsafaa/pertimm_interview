## Structure of the project 
![Screenshot from 2021-04-11 23-54-49](https://user-images.githubusercontent.com/17393730/114322600-68d5cc00-9b21-11eb-94ba-4c664dfbf5ee.png)

## Objectif
Dans un tableau contenant uniquement des zéros et des uns, trouvez l'index du 0 qui, s'il est converti en 1, fera la plus longue séquence du nombre 1.

## Cloner le projet
```sh
$ git clone https://gitlab.com/amalsafaa/pertimm_interview.git
$ cd Exercice2
```
## Compilation
```sh
$ cd program
$ node run_script.js 1,0,0,1
```
## Test
```sh
$ cd Exercice2
$ cd test
$ node test_find_index.js
```
## Screenshots
Resultat<br/><br/>
![Screenshot from 2021-04-11 23-59-14](https://user-images.githubusercontent.com/17393730/114322705-f1546c80-9b21-11eb-974e-78a70f35afe8.png)<br/>
<br/>Test<br/><br/>
![s](https://user-images.githubusercontent.com/17393730/114323284-c3245c00-9b24-11eb-992a-8ea3f78c9cc4.png)

