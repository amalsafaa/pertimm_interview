// Importation de script
const findIndex = require('../program/script');


// Quelques exemples de test
let inputCases = [
    Tab1 = [1, 0, 0, 1], // doit retourner 2
    Tab2 = [1, 0, 0, 0], // doit retourner 1
    Tab3 = [0, 0, 0, 0], // doit retourner 3
    Tab4 = [0, 1, 0, 1, 0, 1], // doit retourner 4
    Tab5 = [0, 0, 0, 0, 0, 1], // doit retourner 4
    Tab6 = [0, 0, 0, 0, 1, 1], // doit retourner 3
    Tab7 = [0, 0, 0, 1, 0, 1], // doit retourner 4
    Tab8 = [1, 1, 0, 1, 1, 0, 1, 1], // doit retourner 5
    Tab9 = [1, 1, 1, 0, 0, 1, 0, 0, 1, 1], // doit retourner 3
    Tab10 = [1, 1, 1, 0, 1, 1, 0, 1, 1, 1], // doit retourner 6
    Tab11 = [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1] // doit retourner 10
];


// Fonction de test
function test() {

    for (var input_index = 0; input_index < 11; input_index++) {
        console.log("------------------------------------------------------------");
        console.log(`--------------------Test numéro ${input_index+1}--------------------------`);
        console.log("------------------------------------------------------------");
        console.log(inputCases[input_index]);
        console.log(`L'index de 0 qu'on doit remplacer est : ${findIndex(inputCases[input_index])}`);
    };
};

test();