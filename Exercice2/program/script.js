function findIndex(arr) {

    max_count = 0;
    count_nbr_of_one = 0;
    prev_zero_index = 0;
    long_index_of_zero = 0;

    for (var acc_index = 0; acc_index < arr.length; acc_index++) {

        if (arr[acc_index] == 0) {

            if (count_nbr_of_one >= max_count) {
                max_count = count_nbr_of_one;
                long_index_of_zero = acc_index;
            };

            if (arr[acc_index + 1] == 0)
                count_nbr_of_one = 0;

            prev_zero_index = acc_index;

        } else if (arr[acc_index] == 1) {
            count_nbr_of_one++;
        };
    };
    if (count_nbr_of_one >= max_count) {
        long_index_of_zero = prev_zero_index;
    };

    return long_index_of_zero;

};

// Exporter la fonction
module.exports = findIndex;