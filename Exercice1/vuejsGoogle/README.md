# Quasar App (vuejs-google)

A Quasar framwork app for Pertimm interview

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://v1.quasar.dev/quasar-cli/quasar-conf-js).

## Screenshots
![google](https://user-images.githubusercontent.com/17393730/114323804-3fb83a00-9b27-11eb-9b53-f91a81cdf3df.png)